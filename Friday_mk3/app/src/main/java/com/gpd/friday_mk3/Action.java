package com.gpd.friday_mk3;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Action {

    public final String post_adress = "https://friday-paul-grbnv.appspot.com";
    public final String http_adress = "https://friday-paul-grbnv.appspot.com/2";
    public String mes;
    public String id;
    public String res = "";
    public String[] answer = {"",""};
    public String flag_todo = "none";
    public String hist = "";
    public String answer_speak;
    public Boolean is_changed;
    Map<String, String> remeber_list = new HashMap<>();

    public String start_action(String[] textr){



        Log.e("STARTACION","Ok");
        flag_todo = "none";
        mes = "";
        res = "";
        Boolean flag_word_esc = true;
        Integer[] check = {0,0,0};
        is_changed = false;
    if (textr.length > 1 ){
        for (String word : textr) {
            if (word.matches("запомни") || word.matches("запиши")){
                if (flag_todo == "none"){flag_todo = "remember";}
            }
            if (word.matches("посчитай")||word.matches("x")|| word.matches("умножить")|| word.matches("/") || word.matches("разделить") || word.matches("делить") || (Objects.equals((" "+word)," +")) || word.matches("плюс") || word.matches("-")|| word.matches("минус")|| word.matches("корень")|| word.matches("степень") || word.matches("степени")|| word.matches("модуль")|| word.matches("модулю")|| word.matches("синус")|| word.matches("косинус")|| word.matches("тангенс")|| word.matches("котангенс")|| word.matches("sin")|| word.matches("cos")|| word.matches("tg")|| word.matches("ctg")){  //ключевые слова
                if (flag_todo == "none"){flag_todo = "calculator";
                Log.e("PLUSS_ERROR","."+" "+word );
                }

            }

            if (word.matches("мои")|| word.matches("записи")){
                Log.e("MY_LIST","satrting_it");
                if (flag_todo=="none"){
                    check[1] = check[1]+1;
                    if (check[1] >1){
                        flag_todo = "my_notes";

                    }

                }
            }
            if (word.matches("прочитай") || word.matches("запись")){
                if (flag_todo == "none") {
                    check[2] = check[2] + 1;
                    if (check[2] > 1) {
                        mes = mes + ">";
                        word = "";
                        flag_todo = "read_notes";

                    }
                }
            }


            if (remeber_list.size()>0) {
                for (String key : remeber_list.keySet()) {
                    if (word.matches(key)) {
                        word = remeber_list.get(key);
                        is_changed = true;

                    }
                }
            }
            if (flag_word_esc &(word.matches("пятница")||word.matches("Пятница"))){
                flag_word_esc = false;
            }else {
                mes = mes + word + " ";
            }
        }

        //ПЕРЕХОД К АЛГОРИТМУ ОБРАБОТКИ !!!!!!! text = mes
    }
     Log.e("MESSAGE",mes);

    if (flag_todo == "calculator"){
        Log.e("CALCULATOR","start - "+mes);
        calculator ();

    }
    if (flag_todo == "remember"){
        Log.e("REMEMBER","start");
        remember();

    }

    if (flag_todo == "my_notes"){
        Log.e("MY_NOTES","starting_act");
        answer[0] = "CODE_MY_NOTES";
        for (String key : remeber_list.keySet()) {
            answer[1] = key + " -|- "+answer[1];
        }
        if (answer[1] == ""){
            answer[1] = "Нет записей.";
        }
    }
    if (flag_todo == "read_notes"){
        Log.e("READ_NOTES","starting_to_read");
        read_notes();
    }
    if (flag_todo == "none" ){
        if (mes.length() > 2){
        answer[0] = "CODE_OK";
        Log.e("SERVER","start - "+mes);
        server(mes);}
        else{
            answer[0] = "CODE_OK";
            answer[1]= "1";
        }
    }

        return "Ok";
    }

    private void server(String text) {
        Log.e("SERVER","Okk");
        Code code = new Code();
        mes = code.code(text);
        id = "1";
        mPost mp = new mPost();
        mp.execute();

    }
    private void calculator(){
        Counter calc = new Counter();
        calc.execute();

    }

    class mPost extends AsyncTask<Object, Object, String> {


        protected void onPreExecute() {
            super.onPreExecute();
           // get_text.setText("Begining now ///");
        }


        protected String doInBackground(Object... params) {

            Post post = new Post();
            try {
                if (mes.matches("CODE_DELETE") && id.matches("CODE_DELETE_4514")) {

                    post.run(http_adress,mes,id,2);
                    res = "Delete";
                    mes = "";
                    id = "";
                }else {
                    post.run(post_adress, mes, id,1);
                    res = "Success";
                }
            } catch (IOException e) {
                e.printStackTrace();
                res = "BadError!!!";
                answer[0] = "CODE_OK";
                answer[1] = "Проверьте подключение к интернету.";
                answer_speak = "Нет ответа. Проверьте интернет подключение";
            }

            return res;
        }


        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("mPost",res);
            mes = "";
            id = "";

        }

    }
    public void get_result(){
        mGet mg = new mGet();
        mg.execute();
    }
    public void dell_answer(){
        mes ="CODE_DELETE";
        id = "CODE_DELETE_4514";
        mPost mp = new mPost();
        mp.execute();
    }

    class mGet extends AsyncTask<Object, Object, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //get_text.setText("Begin");
        }

        @Override
        protected String doInBackground(Object... params) {

            Get get = new Get();
            DeCode deCode = new DeCode();
            answer[1] = "";
            //String result = " ";
            try {
                answer[0] = "CODE_OK";
                String serv_ans = get.run(http_adress);
                if (serv_ans.length()>2){
                    String[] decode_ans = deCode.decode(serv_ans);
                answer[1] = decode_ans[0];
                answer_speak = decode_ans[1];
                }

            } catch (IOException e) {
                e.printStackTrace();
                answer[0] = "CODE_OK";
                answer[1] = "Ошибка сервера.";
                answer_speak = "Нет ответа. Проверьте интернет подключение";
            }

            return answer[1];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (answer[1].length() >1){
                dell_answer();
            }

        }
    }
    class Counter extends AsyncTask <Void,Void,Void>{
        private String x;
        private String y;
        private String act;
        protected void onPreExecute() {
            super.onPreExecute();
            String nums = "1234567890";
            String point = ".";
            String[] count_text = mes.split(" ");
            x = "";
            y = "";
            act = "";
            Boolean flag = true;
            for (String word : count_text){
                if (x.length()>0){
                    flag = false;
                }
                for (Integer n = 0;n < word.length();n = n+1){
                    for (Integer d = 0; d<nums.length();d = d + 1) {

                        if (Objects.equals(word.charAt(n), nums.charAt(d))) {
                            if (flag) {
                                x = x + nums.charAt(d);
                            } else {
                                y = y + nums.charAt(d);
                            }
                        }
                    }
                        if (Objects.equals(word.charAt(n),point.charAt(0))){
                            if (flag){
                                x = x + ".";
                            }else{
                                y = y + ".";
                            }
                        }

                }
                Log.e("CLACLUL_1",x+ " "+y);

                if (word.matches("x")|| word.matches("умножить")){
                    act = "x";
                }
                if ( word.matches("/") || word.matches("разделить") || word.matches("делить") ){
                    act = "/";
                }
                if ( (Objects.equals((" "+word)," +")) || word.matches("плюс")){
                    act = "+";
                }
                if (word.matches("-")|| word.matches("минус")){
                    act = "-";
                }
                if (word.matches("корень")){
                    act = "r";
                }
                if ( word.matches("степень") || word.matches("степени")){
                    act = "e";
                }
                if (word.matches("модуль")|| word.matches("модулю")){
                    act = "m";
                }
                if (word.matches("синус")|| word.matches("sin")){
                    act = "s";
                }
                if (word.matches("косинус")|| word.matches("cos")){
                    act = "c";
                }
                if (word.matches("тангенс")|| word.matches("tg")){
                    act = "t";
                }
                if (word.matches("котангенс")|| word.matches("ctg")){
                    act = "q";
                }

            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            //answer = mes + " nums - "+x + " and "+y;
            String ans_ok = "";
            if (y == "" & (act == "x" || act == "/" || act == "+" || act == "-" || act == "e")) {
                String z = "-> ";
                String more = "";
                if (hist != "") {
                    for (Integer q = hist.length() - 1; q > 0; q = q - 1) {
                        if (Objects.equals(hist.charAt(q), z.charAt(2))) {
                            break;
                        } else {
                            more = more + hist.charAt(q);
                        }
                    }

                }

                more = new StringBuffer(more).reverse().toString();
                try {
                    Double test1 = Double.valueOf(more);
                    y = x;
                    x = more;
                }catch (NumberFormatException e){
                    x = "";
                    y = "";
                }

            }
            if (x == "" & (act == "r" || act == "m" || act == "s" || act == "c" || act == "t" || act == "q")) {
                String z = "-> ";
                String more = "";
                if (hist != "") {
                    for (Integer q = hist.length() - 1; q > 0; q = q - 1) {
                        if (Objects.equals(hist.charAt(q), z.charAt(2))) {
                            break;
                        } else {
                            more = more + hist.charAt(q);
                        }
                    }

                }
                more = new StringBuffer(more).reverse().toString();
                try {
                    Double test1 = Double.valueOf(more);
                    x = more;
                    y = "";
                }catch (NumberFormatException e){
                    x = "";
                    y = "";
                }

            }
            if (x.length() > 0 & y.length() > 0 & (act == "x" || act == "/" || act == "+" || act == "-" || act == "e")) {
                Log.e("CALCUL","start count");
                String exam = ".";
                Boolean dooble_flag_x = false;
                Boolean dooble_flag_y = false;
                Double fir;
                Double sec;
                for (Integer a =0;a<x.length();a = a+1){
                    if (Objects.equals(x.charAt(a),exam.charAt(0))){
                        dooble_flag_x = true;
                    }
                }
                for (Integer a =0;a<y.length();a = a+1){
                    if (Objects.equals(y.charAt(a),exam.charAt(0))){
                        dooble_flag_y = true;
                    }
                }
                if (dooble_flag_x){
                    fir = Double.valueOf(x);
                }else{
                    x = x+".0";
                    fir = Double.valueOf(x);
                }
                if (dooble_flag_y){
                    sec = Double.valueOf(y);
                }else{
                    y = y+".0";
                    sec = Double.valueOf(y);
                }
                if (act == "x"){
                    Double answer_count = fir*sec;
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "/"){
                    Double answer_count = fir/sec;
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "-"){
                    Double answer_count = fir-sec;
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "+"){
                    Double answer_count = fir+sec;
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "e"){
                    Double answer_count = Math.pow(fir, sec);
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }

            }
            if (x.length() > 0 & (act == "r" || act == "m" || act == "s" || act == "c" || act == "t" || act == "q")) {
                Boolean dooble_flag_x = false;
                String exam = ".";
                Double fir;
                for (Integer a =0;a<x.length();a = a+1){
                    if (Objects.equals(x.charAt(a),exam.charAt(0))){
                        dooble_flag_x = true;
                    }
                }
                if (dooble_flag_x){
                    fir = Double.valueOf(x);
                }else{
                    x = x+".0";
                    fir = Double.valueOf(x);
                }
                if (act == "r"){
                    Double answer_count = Math.pow(fir,0.5);
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "m"){
                    Double answer_count = Math.abs(fir);
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "s"){
                    Double answer_count = Math.toDegrees(Math.asin(fir));
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "c"){
                    Double answer_count = Math.toDegrees(Math.acos(fir));
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "t"){
                    Double answer_count = Math.toDegrees(Math.atan(fir));
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }
                if (act == "q"){
                    Double answer_count =Math.toDegrees(Math.atan(1/fir));
                    answer[0] = "CODE_OK";
                    ans_ok = answer_count.toString();
                }

            }
            //Log.e("CHECKING",x+" "+y);
            if (x.length()==0 & y.length() ==0){
                answer[0] = "CODE_OK";
                answer[1] = "Прерванный ряд вычислений. ";
                answer_speak = "Прерванный ряд вычислений.";
            }else {
                Boolean flag1 = false;
                String mystring = "";
                String mystring1 = "";
                String exam2 = ".";
                for (Integer w = 0; w < ans_ok.length(); w = w + 1) {
                    if (Objects.equals(ans_ok.charAt(w), exam2.charAt(0))) {
                        flag1 = true;
                    }
                    if (!flag1) {
                        mystring1 = mystring1 + ans_ok.charAt(w);
                    }
                    if (flag1 & mystring.length() < 4) {
                        mystring = mystring + ans_ok.charAt(w);
                    }
                }
                if (Objects.equals(mystring, ".0")||Objects.equals(mystring, ".000")) {
                    answer_speak = "Ответ " + mystring1;
                    answer[1] = mystring1;
                } else {
                    answer_speak = "Ответ "+mystring1 + " точка "+ mystring;
                    answer[1] = mystring1+mystring;
                }
                
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            act = "";
            x = "";
            y = "";
            hist = "";
        }
    }
    public void remember(){
        String remember_name = "";
        String[] rem_text = mes.split(" ");
        for (String word : rem_text){
            if (!(word.matches("запомни")) & !(word.matches("запиши"))){
                remember_name = remember_name + " " + word;
            }
        }
        if (remember_name.length() < 2 || remember_name == null){remember_name = " переменная";}
        remember_name = remember_name.substring(1);
        Log.e("REMEMBERMYNAME",remember_name);
        remeber_list.put("NEW_NAME",remember_name);
        answer[0] = "CODE_REMEMBER";
        answer[1] = "1234";
        Log.e("CODE_REMEMBER","activated");
    }

    public void read_notes(){
       String[] read_it = mes.split(">");
       answer[0] = "CODE_OK";
        if (is_changed){
        answer[1] = read_it[1];
        answer_speak = read_it[1];
        }else{
            read_it[1] = read_it[1].substring(1,read_it[1].length()-1);
            answer[1] = remeber_list.get(read_it[1]);
            answer_speak = answer[1];
            Log.e("READ_MY_NOTES","ty"+read_it[1]+"ty");
            if (answer[1] == null){
                answer[1] = "Запись не найдена.";
                answer_speak = "Запись не найдена.";
                }
            }

        Log.e("READ_NOTES","loop_complited");
    }




}
