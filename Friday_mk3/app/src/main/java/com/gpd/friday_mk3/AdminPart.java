package com.gpd.friday_mk3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class AdminPart extends AppCompatActivity implements TextToSpeech.OnInitListener {
    public TextView status_text;
    public TextView answer_text;
    public TextView request_text;
    public CheckBox speaker_check;
    public Button drop_bd;
    public SpeechRecognizer speech = null;
    public Action action;
    public AudioManager AudioControl;
    public Intent RecIntent;
    public Boolean need_asw = false;
    public SharedPreferences FridayBD;
    public static final String FILE_BD = "FridayDataBase";
    public final String FILE_BD_LENGTH = "length";
    public final String FILE_BD_REMEMBER = "remember";
    public Integer coef = 0;
    public ScrollView scroll;
    public String voice_answer;
    public Integer speaker_volume = 6;
    private TextToSpeech TtoS;
    private Context x;
    private Boolean recogn_status = false;
    private Menu menu;
    Timer SpeakTimer;
    Friday_Speak f_speak;
    Timer timer;
    TimerTask fTimerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_part);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        status_text = (TextView) findViewById(R.id.listen_text);
        answer_text = (TextView) findViewById(R.id.answer_text);
        request_text = (TextView) findViewById(R.id.command_text);
        drop_bd = (Button) findViewById(R.id.drop_bd);
        scroll = (ScrollView) findViewById(R.id.scroll_text);
        speaker_check = (CheckBox)  findViewById(R.id.checkBoxSpeaker);
        speaker_check.setChecked(true);

        status_text.setBackgroundColor(Color.argb(255,250,141,130));
        x = this;
        action = new Action();

        TtoS =  new TextToSpeech(this,this);

        AudioControl = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        FridayBD = getSharedPreferences(FILE_BD, Context.MODE_PRIVATE);
        if(FridayBD.contains(FILE_BD_LENGTH)) {
            coef = Integer.parseInt(FridayBD.getString(FILE_BD_LENGTH,""));
        }
        updateAnswer();
        if (FridayBD.contains(FILE_BD_REMEMBER)){
            String rem_key = FridayBD.getString(FILE_BD_REMEMBER,"");
            if( rem_key.length()>0){
                rem_key = rem_key.substring(1);
                String[] remember_key = rem_key.split("\\|");
                for (String s : remember_key){

                    String value = FridayBD.getString(s,"");
                    action.remeber_list.put(s,value);
                }
            }
        }

        View.OnClickListener BtnDellBD = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dellBD();

            }
        };
         drop_bd.setOnClickListener(BtnDellBD);
        scroll.fullScroll(View.FOCUS_DOWN);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin_part, menu);
        this.menu = menu;

        MenuItem vol_but = menu.findItem(R.id.volume_6);
        vol_but.setTitle("volume 2 is checked");
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.speech_rec_start) {
            if (recogn_status){
                recogn_status=false;
                stopSpeechRecognition();
                status_text.setBackgroundColor(Color.argb(255,250,141,130));
                status_text.setText("   Press mike to start ..");
                MenuItem micButton = menu.findItem(R.id.speech_rec_start);
                micButton.setIcon(getResources().getDrawable(R.drawable.ic_red_microp));
                if (speaker_check.isChecked()){
                Speak("Управление голосом выключено.");}


               // if (need_asw){
               //     loop_check_for_asw();// NONONONONONONO
                //}

            }
            else{
                startSpeechRecognition();
                status_text.setBackgroundColor(Color.argb(255, 141, 250, 170));
                status_text.setText("   Listening ..");
                MenuItem micButton = menu.findItem(R.id.speech_rec_start);
                micButton.setIcon(getResources().getDrawable(R.drawable.ic_green_micr));
                if (speaker_check.isChecked()){
                Speak("Управление голосом включено.");}
                recogn_status = true;
            }
            return true;
        }
        if (id == R.id.volume_4) {
            whitening_vol_but();
            speaker_volume = 4;
            MenuItem vol_but = menu.findItem(R.id.volume_4);
            vol_but.setTitle("volume 1 is checked");

        }
        if (id == R.id.volume_6) {
            whitening_vol_but();
            speaker_volume = 6;
            MenuItem vol_but = menu.findItem(R.id.volume_6);
            vol_but.setTitle("volume 2 is checked");
        }
        if (id == R.id.volume_8) {
            whitening_vol_but();
            speaker_volume = 8;
            MenuItem vol_but = menu.findItem(R.id.volume_8);
            vol_but.setTitle("volume 3 is checked");
        }
        if (id == R.id.volume_10) {
            whitening_vol_but();
            speaker_volume = 10;
            MenuItem vol_but = menu.findItem(R.id.volume_10);
            vol_but.setTitle("volume 4 is checked");
        }
        if (id == R.id.volume_12) {
            whitening_vol_but();
            speaker_volume = 12;
            MenuItem vol_but = menu.findItem(R.id.volume_12);
            vol_but.setTitle("volume 5 is checked");
        }


        return super.onOptionsItemSelected(item);
    }


    public void onInit (int status) {
        // TODO Auto-generated method stub
        if (status == TextToSpeech.SUCCESS) {
            Locale locale = new Locale("ru");
            int result = TtoS.setLanguage(locale);
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This language is not supported !");
            } else {
                Log.e("TTS", "Ok !!");}
        }
        else {
            Log.e("TTS", "ERROR!");
        }

    }

    public void startSpeechRecognition(){
        speaker_off();
        SpeechRecognition();
        speech.startListening(RecIntent);



    }
    public void stopSpeechRecognition(){
        if (speech != null) {
            speech.stopListening();
            speech.destroy();
        }
    }

    public void start_action (String text){
        if (check_for_sign(text)){
            text = text.toLowerCase();
            request_text.setText("    "+text);
            String[] textr = text.split(" ");
            clear_it();
            action.start_action(textr);
            action.hist = FridayBD.getString(coef.toString(),"");
            need_asw = true;


        }


    }

    public static boolean check_for_sign (String line) {
        String[] strings = line.split(" "); // делим строку на отдельные слова
        for (String word : strings) {

            if (word.matches("пятница") || word.matches("Пятница")) {  // проверяем в цикле каждое отдельное слово
                return true;
            }
        }
        return false;

    }


    class checking_for_asw extends AsyncTask<Void,Void,Void>{
        protected void onPreExecute() {
            super.onPreExecute();
            timer = new Timer();
            fTimerTask = new CheckForAswTimerTask();
            need_asw = false;
        }

        @Override
        protected Void doInBackground(Void... params) {
            timer.schedule(fTimerTask, 200, 1000);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.e("ASYNKTASKADMIN","exit");

        }
    }

    class CheckForAswTimerTask extends TimerTask {
        Integer ask_counter = 0;
        public void run() {
            if (action.res == "Success") {
                ask_counter = ask_counter + 2;
                action.get_result();
            } else {
                Log.e("CHECK_FOR_ASW_TIMER"," Success inside");
                ask_counter = ask_counter + 10;
                if (action.res == "Delete"){
                    ask_counter = ask_counter - 10;
                }

            }

            if ((action.answer[1].length() > 0) || (ask_counter > 50)){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        set_answer(ask_counter);
                    }
                });
            }
        }
    }

    public void set_answer(Integer counter){
        String a = (String) answer_text.getText();
        String b = (String) request_text.getText();
        if (action.answer[0] == "CODE_REMEMBER"){
            stopSpeechRecognition();
            Intent voiceIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            voiceIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            voiceIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "FRIDAY: Say something...");
            voiceIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 0);
            voiceIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                    this.getPackageName());
            timer.cancel();
            Log.e("STARTING","rec_int");
            startActivityForResult(voiceIntent, 1512);


        }
        if (action.answer[0] == "CODE_MY_NOTES"){
            Intent my_notes_act = new Intent(this, NotesActivity.class);
            timer.cancel();
            need_asw = false;
            action.remeber_list.clear();
            action.answer_speak = "";
            String myText = "\n" + "    " + b + "\n" + "    -> " + action.answer[1];
            request_text.setBackgroundColor(Color.argb(255, 238, 238, 238));
            saveHistory(myText);
            updateAnswer();
            timer.cancel();
            need_asw = false;
            action.res = "";
            action.answer[0] = "";
            action.answer[1] = "";
            action.answer_speak = "";
            clear_it();
            startActivity(my_notes_act);
        }
        if (action.answer[0] == "CODE_OK") {
            Log.e("ANSWER_OKOK",action.answer[1]);
            if (counter <= 50 & action.answer[1].length() > 1) {
                Log.e("NO_CODE_ANS","starting");
                String myText = "\n" + "    " + b + "\n" + "    -> " + action.answer[1];
                request_text.setBackgroundColor(Color.argb(255, 238, 238, 238));
                saveHistory(myText);
                updateAnswer();
                voice_answer = action.answer_speak;


            } else {
                String myText = a + "\n" + "\n" + "    " + b + "\n" + "    -> I have no answer now.";
                request_text.setBackgroundColor(Color.argb(255, 238, 238, 238));
                answer_text.setText(myText);
                voice_answer = "Ответ не найден";
            }

            timer.cancel();
            scroll.fullScroll(View.FOCUS_DOWN);
            need_asw = false;
            action.res = "";
            action.answer[0] = "";
            action.answer[1] = "";
            action.answer_speak = "";
            clear_it();
            if (speaker_check.isChecked()) {
                stopSpeechRecognition();
                SpeakTimer = new Timer();
                f_speak = new Friday_Speak();
                request_text.setBackgroundColor(Color.argb(255, 255, 144, 166));
                SpeakTimer.schedule(f_speak, 200);

            }
        }

    }
    class Friday_Speak  extends TimerTask{
        public void run(){
            speaker_on();
            Log.e("TTS", voice_answer);
            TtoS.speak(voice_answer, TextToSpeech.QUEUE_FLUSH, null);
            scroll.fullScroll(View.FOCUS_DOWN);
            Log.e("TTS", "will sleep");
            Integer timeI = 5;
            if (voice_answer.length() <= 20){timeI = 4;}
            if (voice_answer.length() < 60 && voice_answer.length()>20) {timeI = 8;}
            if (voice_answer.length() >= 60 && voice_answer.length()<100){timeI = 15;}
            if (voice_answer.length() >= 100){timeI = 20;}
            try {
                TimeUnit.SECONDS.sleep(timeI);
            } catch (InterruptedException e) {
                Log.e("ERROR_IN_STOPPING", "omg");
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    voice_answer = "";
                    scroll.fullScroll(View.FOCUS_DOWN);
                    request_text.setBackgroundColor(Color.argb(255, 238, 238, 238));
                    if (recogn_status){startSpeechRecognition();}
                }
            });
        }
    }
    public  void Speak(String say_it){
        speaker_on();
        TtoS.speak(say_it, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void saveHistory(String text){
        Editor editor = FridayBD.edit();
        coef = coef + 1;
        editor.putString(FILE_BD_LENGTH, coef.toString());
        editor.apply();
        editor.putString(coef.toString(),text);
        editor.apply();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("REMEMBER","ok_ok");
        need_asw = false;
        action.res = "";
        action.answer[0] = "";
        action.answer[1] = "";
        action.answer_speak = "";
        if (requestCode == 1512 && resultCode == RESULT_OK) {
            ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String rem_it = "";
            if (matches.size() > 0) {
                rem_it = (String) matches.get(0);
            }
            else{
                rem_it = "ничего нет.";
            }
            Log.e("REMEMBERING_IT", rem_it);
            if (rem_it.length() > 0 & rem_it!= "ничего нет.") {
                String new_name = action.remeber_list.get("NEW_NAME");
                action.remeber_list.put(new_name, rem_it);
                Editor editor = FridayBD.edit();
                String bd_rem = FridayBD.getString(FILE_BD_REMEMBER, "");
                editor.putString(FILE_BD_REMEMBER, bd_rem + "|" + new_name);
                editor.apply();
                editor.putString(new_name, rem_it);
                editor.apply();
                saveHistory("\n       Запись : "+new_name+"\n "+"    -> "+rem_it);
                request_text.setBackgroundColor(Color.argb(255, 238, 238, 238));
                updateAnswer();
                if (speaker_check.isChecked()) {
                    voice_answer = "Запись сохранена";
                    SpeakTimer = new Timer();
                    f_speak = new Friday_Speak();
                    request_text.setBackgroundColor(Color.argb(255, 255, 144, 166));
                    SpeakTimer.schedule(f_speak, 150);
                }else{
                    if (recogn_status){startSpeechRecognition();}
                }
            }else{
                voice_answer = "Ошибка в сохранении записи.";
                SpeakTimer = new Timer();
                f_speak = new Friday_Speak();
                request_text.setBackgroundColor(Color.argb(255, 255, 144, 166));
                SpeakTimer.schedule(f_speak, 150);

            }

        }else{
            voice_answer = "Ошибка в сохранении записи.";
            String a = (String) answer_text.getText();
            String b = (String) request_text.getText();
            String myText = a + "\n" + "\n" + "    " + b + "\n" + "    -> "+voice_answer;
            request_text.setBackgroundColor(Color.argb(255, 238, 238, 238));
            answer_text.setText(myText);
            SpeakTimer = new Timer();
            f_speak = new Friday_Speak();
            request_text.setBackgroundColor(Color.argb(255, 255, 144, 166));
            SpeakTimer.schedule(f_speak, 150);


        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void updateAnswer(){
        String text_a = "";
        text_a = text_a + "-------▂▃▅▇█▓▒░۩۞۩ -|- ۩۞۩░▒▓█▇▅▃▂--------";
        if (coef > 0){
            for (Integer ind = 0; ind <= coef; ind = ind + 1) {
                String hist_asw = FridayBD.getString(ind.toString(), "");
                text_a = text_a + hist_asw;
            }
            answer_text.setText(text_a);
        }else {
            answer_text.setText(R.string.command_text);
        }

        scroll.fullScroll(View.FOCUS_DOWN);



    }
    public  void dellBD(){

        Editor editor = FridayBD.edit();
        for (Integer i = coef;i>0;i = i - 1){
            editor.remove(i.toString());
            editor.apply();
        }
        Log.e("DELL_BD",FridayBD.getString(FILE_BD_LENGTH,""));
        editor.putString(FILE_BD_LENGTH,"0");
        editor.apply();

        //editor.clear();
        //editor.commit();
        //editor.apply();

        coef = 0;
        updateAnswer();
        Toast.makeText(this, "История удалена.", Toast.LENGTH_SHORT).show();
    }
    public void clear_it (){

        if (FridayBD.contains(FILE_BD_REMEMBER)){
            action.remeber_list.clear();
            String rem_key = FridayBD.getString(FILE_BD_REMEMBER,"");
            if( rem_key.length()>0){
                rem_key = rem_key.substring(1);
                String[] remember_key = rem_key.split("\\|");
                for (String s : remember_key){

                    String value = FridayBD.getString(s,"");
                    action.remeber_list.put(s,value);
                }
            }
        }else{action.remeber_list.clear();}
    }

    public void SpeechRecognition(){
        speech = SpeechRecognizer.createSpeechRecognizer(x);
        speech.setRecognitionListener(listen); //Создание объекта распознавателя речи
        RecIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        RecIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                this.getPackageName());

        RecIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        RecIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        //RecIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Talk please .....");


    }
    RecognitionListener listen = new RecognitionListener() {
        @Override
        public void onBeginningOfSpeech() {
        }
        @Override
        public void onBufferReceived(byte[] buffer) {
        }
        @Override
        public void onEndOfSpeech() {
        }
        @Override
        public void onError(int errorCode) {

            String errorMessage = getErrorText(errorCode); // Вызов метода расшифровки ошибки
            Log.e("RECOGNITION_ERROR",errorMessage);
            stopSpeechRecognition();
            if (recogn_status){startSpeechRecognition();}
        }
        @Override
        public void onEvent(int arg0, Bundle arg1) {
        }
        @Override
        public void onPartialResults(Bundle arg0) {
        }
        @Override
        public void onReadyForSpeech(Bundle arg0) {
        }
        public void onResults(Bundle results) {

            ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            String ans = data.get(0);  //Несколько вариантов распознавания.!!!!

            start_action(ans);

            if (need_asw){
                scroll.fullScroll(View.FOCUS_DOWN);
                request_text.setBackgroundColor(Color.argb(255, 130, 160, 250));
                //answer_text.setText("Getting answer ..");
                checking_for_asw  check = new checking_for_asw();
                check.execute();

            }
            stopSpeechRecognition();

            if (recogn_status){startSpeechRecognition();}

        }
        @Override
        public void onRmsChanged(float rmsdB) {
        }

    };
    public static String getErrorText(int errorCode) { // Метод возврата ошибки по ее коду
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Ошибка сети.";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "Нет совпадений.";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "Waiting ... ";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }
    public void speaker_on()
    {
        AudioControl.setStreamVolume(AudioManager.STREAM_MUSIC, speaker_volume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
    }
    public void speaker_off() // Метод включения внешних динамиков.
    {
        AudioControl.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
    }
    public void whitening_vol_but(){
        if (speaker_volume == 4){
            MenuItem vol_but = menu.findItem(R.id.volume_4);
            vol_but.setTitle("volume 1");
        }
        if (speaker_volume == 6){
            MenuItem vol_but = menu.findItem(R.id.volume_6);
            vol_but.setTitle("volume 2");
        }
        if (speaker_volume == 8){
            MenuItem vol_but = menu.findItem(R.id.volume_8);
            vol_but.setTitle("volume 3");
        }
        if (speaker_volume == 10){
            MenuItem vol_but = menu.findItem(R.id.volume_10);
            vol_but.setTitle("volume 4");
        }
        if (speaker_volume == 12){
            MenuItem vol_but = menu.findItem(R.id.volume_12);
            vol_but.setTitle("volume 5");
        }
    }

    public void onDestroy() {
        // Don't forget to shutdown TTS!
        if (TtoS != null) {
            TtoS.stop();
            TtoS.shutdown();
        }

        super.onDestroy();
    }
}
