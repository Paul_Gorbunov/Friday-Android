package com.gpd.friday_mk3;


import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


public class Post {

    OkHttpClient client = new OkHttpClient();

    //String json = "{“name”:”Ivan”, “surname”:”Petrov”, “age”:”40”}";
    //RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

    RequestBody body;
    String run(String url,String mes,String id,Integer react) throws IOException {
        //RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
        if (1 == react) {body =new FormBody.Builder()
                .add("key", "3")
                .add("mes", mes)
                .add("id",id)
                .build();}
        if (2 == react) {body =new FormBody.Builder()
                .add("dell", "2").build();}

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
