package com.gpd.friday_mk3;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NotesActivity extends AppCompatActivity {
    public SharedPreferences FridayBD;
    public static final String FILE_BD = "FridayDataBase";
    public final String FILE_BD_LENGTH = "length";
    public final String FILE_BD_REMEMBER = "remember";
    Map<String, String> rem_list = new HashMap<>();
    public String text = "";
    public Integer i = 0;
    public ArrayList list_key = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.notes_screen_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        set_ellem();


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private View.OnClickListener btnclick = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            Integer but_id = view.getId();
            String my_key = list_key.get(but_id).toString();
            Log.e("NOTES_ACTIVITY","press_but "+but_id);
            SharedPreferences.Editor editor = FridayBD.edit();
            Log.e("LISY_KEY",my_key+" "+but_id.toString());
            editor.remove(my_key);
            editor.apply();
            editor.remove(FILE_BD_REMEMBER);
            editor.apply();
            list_key.remove(my_key);

            for (Integer o = 0;o<list_key.size();o = o + 1){
                String bd_rem = FridayBD.getString(FILE_BD_REMEMBER, "");
                editor.putString(FILE_BD_REMEMBER, bd_rem + "|" + list_key.get(o));
                editor.apply();
            }
            String bd_rem = FridayBD.getString(FILE_BD_REMEMBER, "");
            Log.e("SHARED_REMEMBER",bd_rem);
            rem_list.clear();
            list_key.clear();
            text = "";
            LinearLayout layout = (LinearLayout) findViewById(R.id.linear_notes);
            layout.removeAllViews();
            set_ellem();


        }
    };

    public void set_ellem(){
        i = 0;
        FridayBD = getSharedPreferences(FILE_BD, Context.MODE_PRIVATE);

        if (FridayBD.contains(FILE_BD_REMEMBER)){
            String rem_key = FridayBD.getString(FILE_BD_REMEMBER,"");
            if( rem_key.length()>0){
                rem_key = rem_key.substring(1);
                String[] remember_key = rem_key.split("\\|");
                for (String s : remember_key){

                    String value = FridayBD.getString(s,"");
                    rem_list.put(s,value);
                }
            }
        }
        LinearLayout scroll = (LinearLayout) findViewById(R.id.linear_notes);
        if (rem_list.size()>0) {
            for (String key : rem_list.keySet()) {
                text = rem_list.get(key);

                TextView textView = new TextView(this);
                textView.setLayoutParams(new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT));
                textView.setTextSize(18);
                textView.setTextColor(Color.BLACK);
                textView.setText(" -> "+key+"\n   "+text);
                scroll.addView(textView);
                Button button = new Button(this);
                button.setLayoutParams(new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT));
                button.setText("Удалить запись: "+key);
                button.setId(i);
                scroll.addView(button);
                button.setOnClickListener(btnclick);
                list_key.add(key);

                i = i + 1;


            }
        }

    }

}
