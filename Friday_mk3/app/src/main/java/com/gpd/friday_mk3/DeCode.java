package com.gpd.friday_mk3;

import android.util.Log;

public class DeCode {
    public String[] decode(String text){
        String prom_res = "";
        String[] m_text = text.split(">>");
        for (String ser_text : m_text){
            prom_res = prom_res + ser_text;
        }
        String[] my_text = prom_res.split(" ");
        String alp = "абвгдежзийклмнопрстуфхцчшщъыьэюя";
        String alp2 = "abcdefghijklmnopqrstuvwxyz.,+-/:;()&%_=?";
        String a_t = "";
        String adress = "";
        String say = "";
        for (Integer x = 0; x < (my_text.length); x = x + 1) {
            try {
                Integer w = Integer.parseInt(my_text[x]);
                a_t = a_t + alp.charAt(w);
                } catch (NumberFormatException e) {
                    if (my_text[x].contains("!")) {
                        if (my_text[x].length() > 1) {
                            a_t = a_t + my_text[x].substring(1);
                        }
                    }
                    if (my_text[x].contains("<>")) {
                        a_t = a_t + " ";
                    }
                    if (my_text[x].contains("(&)")) {
                       a_t = a_t + "°";
                     }
                    if (alp2.contains(my_text[x])) {
                        a_t = a_t + my_text[x];
                    }

            }
        }
        if (a_t.contains("-&&-")){
            String[] res = a_t.split("-&&-");
            a_t = res[0];
            adress = res[1];
        }

        if (a_t.contains(".")){
            Log.e("A_T",a_t);
            String[] es = a_t.split("\\.");
            if (es[0].length()>40) {
                say = es[0];
                if (es.length > 1) {
                    if (es[1].length() < 15) {
                        say = say + es[1];
                    }
                }
            }
            else {
                    if (es.length > 1) {
                        if (es[1].length() < 30) {
                            say = es[0] + es[1];
                        } 
                    }else {
                        say = es[0];
                    }
                }

        }
        if (a_t.contains("/-/")){
            String[] es = a_t.split("/-/");
            say = es[0];
            if(es.length>1) {
                if (es[1].length() < 30) {
                    say = say + es[1];
                }
            }
        }

        if (say == ""){
            say = a_t;
        }
        if (a_t.contains("big error") || a_t.contains("bad error")){
            a_t = "Ответ не удалось найти.";
            say = a_t;
        }



        //say = "Ответ получен.";

        String[] send = {a_t,say,adress};
        return send;
    }


}
